class NoAvailableFundsError(Exception):
    pass


class InvalidLoanAmountError(Exception):
    pass