from lenderrepository import LenderRepository
from errors import NoAvailableFundsError, InvalidLoanAmountError


class Quote(object):
    def __init__(self, rate=0):
        self.rate = rate


class QuoteCalculator(object):
    lenderRepository = LenderRepository()

    def get_quote(self, loan_amount):
        if loan_amount % 100 != 0:
            raise InvalidLoanAmountError()
        lenders = self.lenderRepository.get_lenders()
        if loan_amount > sum(l.fund for l in lenders):
            raise NoAvailableFundsError()
        remaining_amount = loan_amount
        collective_interest = 0
        sorted_lenders = sorted(lenders, key=lambda l: l.rate)
        sorted_lenders = sorted(sorted_lenders, key=lambda l: l.fund * -1)
        for lender in sorted_lenders:
            available_fund = min(lender.fund, remaining_amount)
            collective_interest += available_fund * lender.rate
            remaining_amount -= available_fund
            if remaining_amount == 0:
                break

        return Quote(rate=round(collective_interest / loan_amount, 2))
