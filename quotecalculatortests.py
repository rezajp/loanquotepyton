import unittest
from unittest.mock import patch

from errors import NoAvailableFundsError, InvalidLoanAmountError
from lender import Lender
from quotecalculator import QuoteCalculator


class QuoteCalculatorTests(unittest.TestCase):
    @patch('lenderrepository.LenderRepository')
    def test_NoAvailableFunds_is_thrown_when_sum_of_lenders_offers_is_less_than_requested_loan(self,
                                                                                               MockLenderRepository):
        lenderRepository = MockLenderRepository()

        lenderRepository.get_lenders.return_value = [Lender(100), Lender(1899)]

        calculator = QuoteCalculator()
        calculator.lenderRepository = lenderRepository
        with self.assertRaises(NoAvailableFundsError):
            calculator.get_quote(2000)

    def test_InvalidLoanAmount_is_thrown_when_loan_amount_is_not_in_100s(self):
        calculator = QuoteCalculator()
        with self.assertRaises(InvalidLoanAmountError):
            calculator.get_quote(2110)

    @patch('lenderrepository.LenderRepository')
    def test_7_percent_rate_is_proposed_when_amount_available_and_best_rate_is_7(self, MockLenderRepository):
        lenderRepository = MockLenderRepository()

        lenderRepository.get_lenders.return_value = [Lender(rate=0.075, fund=640), Lender(rate=0.069, fund=480),
                                                     Lender(rate=0.071, fund=520), Lender(rate=0.104, fund=170),
                                                     Lender(rate=0.081, fund=320), Lender(rate=0.074, fund=140),
                                                     Lender(rate=0.071, fund=60)]

        calculator = QuoteCalculator()
        calculator.lenderRepository = lenderRepository
        quote = calculator.get_quote(2000)
        self.assertNotEqual(quote.rate, 7.00)


if __name__ == '__main__':
    unittest.main()
